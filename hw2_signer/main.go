package main

import (
	"fmt"
	"sort"
	"strings"
	"sync"
)

func Crc32Async(data string) chan string {
	out := make(chan string, 1)
	go func(data string, out chan<- string) {
		out <- DataSignerCrc32(data)
	}(data, out)
	return out
}
func Md5Async(data string, qCh chan interface{}) chan string {
	out := make(chan string, 1)
	go func(data string, o chan<- string, q chan interface{}) {
		q <- struct{}{}
		o <- DataSignerMd5(data)
		<-q
	}(data, out, qCh)
	return out
}

func SingleHash(in, out chan interface{}) {
	wg := &sync.WaitGroup{}
	quotCh := make(chan interface{}, 1)
	for val := range in {
		wg.Add(1)
		go func(v interface{}, o chan interface{}, wg *sync.WaitGroup, qCh chan interface{}) {
			data := fmt.Sprintf("%v", v)
			ch1 := Crc32Async(data)
			ch2 := Md5Async(data, qCh)
			ch3 := Crc32Async(<-ch2)
			result := <-ch1 + "~" + <-ch3
			o <- result
			wg.Done()
		}(val, out, wg, quotCh)
	}
	wg.Wait()
}

func MultiHash(in, out chan interface{}) {
	wg := &sync.WaitGroup{}
	for val := range in {
		wg.Add(1)
		go func(v interface{}, o chan interface{}, wgg *sync.WaitGroup) {
			defer wgg.Done()
			data := fmt.Sprintf("%v", v)
			count := 6
			parts := make([]string, count, count)
			wg := &sync.WaitGroup{}
			for i := 0; i < count; i++ {
				wg.Add(1)
				go func(data string, wg *sync.WaitGroup, i int) {
					parts[i] = DataSignerCrc32(fmt.Sprintf("%v%v", i, data))
					wg.Done()
				}(data, wg, i)
			}
			wg.Wait()
			o <- strings.Join(parts, "")
		}(val, out, wg)
	}
	wg.Wait()
}

func CombineResults(in, out chan interface{}) {
	acc := make([]string, 0, 5)
	for val := range in {
		acc = append(acc, fmt.Sprintf("%v", val))
	}
	sort.Slice(acc, func(i, j int) bool {
		return acc[i] < acc[j]
	})
	out <- strings.Join(acc, "_")
}

func ExecutePipeline(jobs ...job) {
	in := make(chan interface{})
	wg := &sync.WaitGroup{}
	left := in
	for idx, j := range jobs {
		// 100 по тз
		out := make(chan interface{}, 100)
		wg.Add(1)
		go func(jb job, l, r chan interface{}, wg *sync.WaitGroup, i int) {
			jb(l, r)
			close(r)
			wg.Done()
		}(j, left, out, wg, idx)
		left = out
	}
	wg.Wait()
}
